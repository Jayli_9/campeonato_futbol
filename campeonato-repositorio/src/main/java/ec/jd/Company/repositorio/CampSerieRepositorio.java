package ec.jd.Company.repositorio;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import ec.jd.Company.modelo.CampSerie;

/**
 * Repositorio para la entidad campCampeonato
 * 
 * @author Jayli De la Torre
 *
 */

public interface CampSerieRepositorio extends JpaRepository<CampSerie, Integer> {

	List<CampSerie> findBySeriEstado(int seriEstado);

}
