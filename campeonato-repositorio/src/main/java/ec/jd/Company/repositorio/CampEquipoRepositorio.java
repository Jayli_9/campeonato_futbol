package ec.jd.Company.repositorio;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ec.jd.Company.modelo.CampEquipo;
/**
* Repositorio para la entidad campCampeonato
* 
* @author Jayli De la Torre
*
*/

public interface CampEquipoRepositorio extends JpaRepository<CampEquipo, Integer>{

	List<CampEquipo>  findByEquiEstado(int equiEstado);

	List<CampEquipo>  findByEquiEstadoAndCampCampeonatoCampCodigoOrderByEquiNombre(int equiEstado, Long campCodigo);
	
	Page<CampEquipo>  findByEquiEstadoAndCampCampeonatoCampCodigoOrderByEquiNombre(int equiEstado, Long campCodigo,Pageable pageable);
	
}
