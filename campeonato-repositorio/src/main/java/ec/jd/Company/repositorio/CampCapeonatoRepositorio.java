package ec.jd.Company.repositorio;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ec.jd.Company.modelo.CampCampeonato;
/**
* Repositorio para la entidad campCampeonato
* 
* @author Jayli De la Torre
*
*/

public interface CampCapeonatoRepositorio extends JpaRepository<CampCampeonato, Integer>{

	List<CampCampeonato>  findByCampEstado(int campEstado);

	Page<CampCampeonato> findByCampEstadoOrderByCampNombre(int estado, Pageable pageable);
}
