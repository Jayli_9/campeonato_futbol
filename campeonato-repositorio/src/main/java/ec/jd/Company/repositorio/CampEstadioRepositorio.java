package ec.jd.Company.repositorio;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ec.jd.Company.modelo.CampEstadio;
/**
* Repositorio para la entidad campCampeonato
* 
* @author Jayli De la Torre
*
*/

public interface CampEstadioRepositorio extends JpaRepository<CampEstadio, Integer>{

	List<CampEstadio>  findByEstaEstado(int estaEstado);
	Page<CampEstadio>  findByEstaEstadoAndCampCampeonatoCampCodigoOrderByEstaNombre(int estaEstado, Long campCodigo,Pageable pageable);
	

}
