package ec.jd.Company.servicio.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ec.jd.Company.modelo.CampEstadio;
import ec.jd.Company.repositorio.CampEstadioRepositorio;
import ec.jd.Company.servicio.CampEstadioServicio;




/**
 * Implementación de los servicios para camCampeonato
 * 
 * @author Jayli De la Torre
 */

@Service
public class CampEstadioServicioImpl implements CampEstadioServicio{
	
	@Autowired
	private CampEstadioRepositorio campEstadioRepositorio;

	@Override
	public List<CampEstadio> listarPorEstado(int estaEstado) {
		// TODO Auto-generated method stub
		return campEstadioRepositorio.findByEstaEstado(estaEstado);
	}

	@SuppressWarnings("null")
	@Override
	public CampEstadio guardarActualizar(CampEstadio campEstadio) {
		// TODO Auto-generated method stub
		return campEstadioRepositorio.save(campEstadio);
	}

	@Override
	public Page<CampEstadio> listarEstadiosPorCampCodigoPaginado(int estaEstado, Long campCodigo, Pageable paginacion) {
		// TODO Auto-generated method stub
		return campEstadioRepositorio.findByEstaEstadoAndCampCampeonatoCampCodigoOrderByEstaNombre(estaEstado, campCodigo, paginacion);
	}

	


}
