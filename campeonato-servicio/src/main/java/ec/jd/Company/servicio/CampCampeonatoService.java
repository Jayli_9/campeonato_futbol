package ec.jd.Company.servicio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ec.jd.Company.modelo.CampCampeonato;

/**
 * Interfaz
 * 
 * @author Jayli De la Torre
 *
 */

public interface CampCampeonatoService {

	List<CampCampeonato> listarPorEstado(int campEstado);

	CampCampeonato guardarActualizar(CampCampeonato camCampeonato);

	Page<CampCampeonato> listarPorEstadoPaginacion(int estado,Pageable paginacion);

}
