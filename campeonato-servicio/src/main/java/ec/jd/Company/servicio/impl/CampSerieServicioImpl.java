package ec.jd.Company.servicio.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ec.jd.Company.modelo.CampSerie;
import ec.jd.Company.repositorio.CampSerieRepositorio;
import ec.jd.Company.servicio.CampSerieServicio;

/**
 * Implementación de los servicios para camCampeonato
 * 
 * @author Jayli De la Torre
 */

@Service
public class CampSerieServicioImpl implements CampSerieServicio {

	@Autowired
	private CampSerieRepositorio campSerieRepositorio;

	@Override
	public List<CampSerie> listarPorEstado(int seriEstado) {
		// TODO Auto-generated method stub
		return campSerieRepositorio.findBySeriEstado(seriEstado);
	}

	@SuppressWarnings("null")
	@Override
	public CampSerie guardarActualizar(CampSerie campSerie) {
		// TODO Auto-generated method stub
		return campSerieRepositorio.save(campSerie);
	}

}
