package ec.jd.Company.servicio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ec.jd.Company.modelo.CampEquipo;


/**
 * Interfaz
 * 
 * @author Jayli De la Torre
 *
 */

public interface CampEquipoServicio {

	List<CampEquipo> listarPorEstado(int equiEstado);
	CampEquipo guardarActualizar(CampEquipo camEquipo);
	List<CampEquipo> listarPorEstadoAndCampCodigo(int equiEstado, Long campCodigo);
	Page<CampEquipo> listarEquiposPorCampCodigoPaginado(int equiEstado, Long campCodigo, Pageable paginacion);
}
