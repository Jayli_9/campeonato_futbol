package ec.jd.Company.servicio;

import java.util.List;
import ec.jd.Company.modelo.CampSerie;

/**
 * Interfaz
 * 
 * @author Jayli De la Torre
 *
 */

public interface CampSerieServicio {

	List<CampSerie> listarPorEstado(int seriEstado);

	CampSerie guardarActualizar(CampSerie campSerie);
}
