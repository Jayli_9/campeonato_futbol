package ec.jd.Company.servicio.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ec.jd.Company.modelo.CampEquipo;
import ec.jd.Company.repositorio.CampEquipoRepositorio;
import ec.jd.Company.servicio.CampEquipoServicio;

/**
 * Implementación de los servicios para camCampeonato
 * 
 * @author Jayli De la Torre
 */

@Service
public class CampEquipoServicioImpl implements CampEquipoServicio {

	@Autowired
	private CampEquipoRepositorio camEquipoRepositorio;

	@Override
	public List<CampEquipo> listarPorEstado(int campEstado) {
		// TODO Auto-generated method stub
		return camEquipoRepositorio.findByEquiEstado(campEstado);
	}

	@SuppressWarnings("null")
	@Override
	public CampEquipo guardarActualizar(CampEquipo campEquipo) {
		// TODO Auto-generated method stub
		return camEquipoRepositorio.save(campEquipo);
	}

	@Override
	public List<CampEquipo> listarPorEstadoAndCampCodigo(int equiEstado, Long campCodigo) {
		// TODO Auto-generated method stub
		return camEquipoRepositorio.findByEquiEstadoAndCampCampeonatoCampCodigoOrderByEquiNombre(equiEstado,
				campCodigo);
	}

	@Override
	public Page<CampEquipo> listarEquiposPorCampCodigoPaginado(int equiEstado, Long campCodigo, Pageable paginacion) {
		// TODO Auto-generated method stub
		return camEquipoRepositorio.findByEquiEstadoAndCampCampeonatoCampCodigoOrderByEquiNombre(equiEstado, campCodigo,
				paginacion);
	}

}
