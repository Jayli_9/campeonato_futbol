package ec.jd.Company.servicio;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ec.jd.Company.modelo.CampEquipo;
import ec.jd.Company.modelo.CampEstadio;


/**
 * Interfaz
 * 
 * @author Jayli De la Torre
 *
 */

public interface CampEstadioServicio  {

	List<CampEstadio> listarPorEstado(int estaEstado);
	CampEstadio guardarActualizar(CampEstadio campEstadio);
	Page<CampEstadio> listarEstadiosPorCampCodigoPaginado(int estaEstado, Long campCodigo, Pageable paginacion);
}
