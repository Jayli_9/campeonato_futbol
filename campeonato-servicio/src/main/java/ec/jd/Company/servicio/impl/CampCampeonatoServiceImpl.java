package ec.jd.Company.servicio.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ec.jd.Company.modelo.CampCampeonato;
import ec.jd.Company.repositorio.CampCapeonatoRepositorio;
import ec.jd.Company.servicio.CampCampeonatoService;




/**
 * Implementación de los servicios para camCampeonato
 * 
 * @author Jayli De la Torre
 */

@Service
public class CampCampeonatoServiceImpl implements CampCampeonatoService{
	
	@Autowired
	private CampCapeonatoRepositorio camCapeonatoRepository;

	@Override
	public List<CampCampeonato> listarPorEstado(int campEstado) {
		// TODO Auto-generated method stub
		return camCapeonatoRepository.findByCampEstado(campEstado);
	}

	@SuppressWarnings("null")
	@Override
	public CampCampeonato guardarActualizar(CampCampeonato camCampeonato) {
		// TODO Auto-generated method stub
		return camCapeonatoRepository.save(camCampeonato);
	}

	@Override
	public Page<CampCampeonato> listarPorEstadoPaginacion(int estado,Pageable paginacion ) {
		// TODO Auto-generated method stub
		return camCapeonatoRepository.findByCampEstadoOrderByCampNombre(estado, paginacion);
	}

	


}
