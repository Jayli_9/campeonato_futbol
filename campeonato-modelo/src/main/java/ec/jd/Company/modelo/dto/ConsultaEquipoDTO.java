package ec.jd.Company.modelo.dto;

public class ConsultaEquipoDTO {

	private int numeroPagina;
	private int numeroItems;
	private Long campCodigo;

	public int getNumeroPagina() {
		return numeroPagina;
	}
	public void setNumeroPagina(int numeroPagina) {
		this.numeroPagina = numeroPagina;
	}
	public int getNumeroItems() {
		return numeroItems;
	}
	public void setNumeroItems(int numeroItems) {
		this.numeroItems = numeroItems;
	}
	public Long getCampCodigo() {
		return campCodigo;
	}
	public void setCampCodigo(Long campCodigo) {
		this.campCodigo = campCodigo;
	}

	
}
