package ec.jd.Company.modelo.dto;

public class InstitucionEstablecimentoRegimenDTO {

	private Integer insestCodigo;
	private String descripcion;
	private Long regCodigo;
	
	public Integer getInsestCodigo() {
		return insestCodigo;
	}
	
	public void setInsestCodigo(Integer insestCodigo) {
		this.insestCodigo = insestCodigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Long getRegCodigo() {
		return regCodigo;
	}
	
	
	public void setRegCodigo(Long regCodigo) {
		this.regCodigo = regCodigo;
	}
	
	
}
