package ec.jd.Company.modelo;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="camp_estadio")
@NamedQuery(name="CampEstadio.findAll", query="SELECT i FROM CampEstadio i")
public class CampEstadio implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO,generator = "native")
	@GenericGenerator(name="native",strategy = "native")
	@Column(name="ESTA_CODIGO")
	private Long estaCodigo;
	
	@Column(name="ESTA_ESTADO")
	private int estaEstado;

	@Column(name="ESTA_DESCRIPCION")
	private String estaDescripcion;

	@Column(name="ESTA_NOMBRE")
	private String estaNombre;
	

	@ManyToOne
	@JoinColumn(name="CAMP_CODIGO")
	private CampCampeonato campCampeonato;


	public Long getEstaCodigo() {
		return estaCodigo;
	}


	public void setEstaCodigo(Long estaCodigo) {
		this.estaCodigo = estaCodigo;
	}


	public int getEstaEstado() {
		return estaEstado;
	}


	public void setEstaEstado(int estaEstado) {
		this.estaEstado = estaEstado;
	}


	public String getEstaDescripcion() {
		return estaDescripcion;
	}


	public void setEstaDescripcion(String estaDescripcion) {
		this.estaDescripcion = estaDescripcion;
	}


	public String getEstaNombre() {
		return estaNombre;
	}


	public void setEstaNombre(String estaNombre) {
		this.estaNombre = estaNombre;
	}


	public CampCampeonato getCampCampeonato() {
		return campCampeonato;
	}


	public void setCampCampeonato(CampCampeonato campCampeonato) {
		this.campCampeonato = campCampeonato;
	}

	
	
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="BLO_FECHA_CREACION")
//	@JsonFormat(pattern = "yyyy-MM-dd")
//	private Date bloFechaCreacion;
//
//	@Column(name="BLO_NEMONICO")
//	private String bloNemonico;
	

}
