package ec.jd.Company.modelo;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="camp_equipo")
@NamedQuery(name="CampEquipo.findAll", query="SELECT i FROM CampEquipo i")
public class CampEquipo implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO,generator = "native")
	@GenericGenerator(name="native",strategy = "native")
	@Column(name="EQUI_CODIGO")
	private Long equiCodigo;
	
	@Column(name="EQUI_ESTADO")
	private int equiEstado;

	@Column(name="EQUI_DESCRIPCION")
	private String equiDescripcion;

	@Column(name="EQUI_NOMBRE")
	private String equiNombre;
	

	@Column(name="EQUI_LUGAR")
	private String equiLugar;

	@Column(name="EQUI_LOGO")
	private byte[] equiLogo;

	@ManyToOne
	@JoinColumn(name="CAMP_CODIGO")
	private CampCampeonato campCampeonato;

	public Long getEquiCodigo() {
		return equiCodigo;
	}

	public void setEquiCodigo(Long equiCodigo) {
		this.equiCodigo = equiCodigo;
	}

	public int getEquiEstado() {
		return equiEstado;
	}

	public void setEquiEstado(int equiEstado) {
		this.equiEstado = equiEstado;
	}

	public String getEquiDescripcion() {
		return equiDescripcion;
	}

	public void setEquiDescripcion(String equiDescripcion) {
		this.equiDescripcion = equiDescripcion;
	}

	public String getEquiNombre() {
		return equiNombre;
	}

	public void setEquiNombre(String equiNombre) {
		this.equiNombre = equiNombre;
	}

	public String getEquiLugar() {
		return equiLugar;
	}

	public void setEquiLugar(String equiLugar) {
		this.equiLugar = equiLugar;
	}

	public byte[] getEquiLogo() {
		return equiLogo;
	}

	public void setEquiLogo(byte[] equiLogo) {
		this.equiLogo = equiLogo;
	}

	public CampCampeonato getCampCampeonato() {
		return campCampeonato;
	}

	public void setCampCampeonato(CampCampeonato campCampeonato) {
		this.campCampeonato = campCampeonato;
	}
	
	
	
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="BLO_FECHA_CREACION")
//	@JsonFormat(pattern = "yyyy-MM-dd")
//	private Date bloFechaCreacion;
//
//	@Column(name="BLO_NEMONICO")
//	private String bloNemonico;
	

}
