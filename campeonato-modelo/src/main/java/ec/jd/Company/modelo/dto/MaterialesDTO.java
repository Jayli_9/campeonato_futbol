package ec.jd.Company.modelo.dto;

public class MaterialesDTO {
	private int insCodigo;
	private int reanleCodigo;
	
	public int getInsCodigo() {
		return insCodigo;
	}
	public void setInsCodigo(int insCodigo) {
		this.insCodigo = insCodigo;
	}
	public int getReanleCodigo() {
		return reanleCodigo;
	}
	public void setReanleCodigo(int reanleCodigo) {
		this.reanleCodigo = reanleCodigo;
	}
	
}
