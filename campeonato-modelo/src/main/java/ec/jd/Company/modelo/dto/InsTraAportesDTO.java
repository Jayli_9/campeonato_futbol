package ec.jd.Company.modelo.dto;

public class InsTraAportesDTO {
	private int apoAnio;
	private int insCodigo;
	
	public int getApoAnio() {
		return apoAnio;
	}
	public void setApoAnio(int apoAnio) {
		this.apoAnio = apoAnio;
	}
	public int getInsCodigo() {
		return insCodigo;
	}
	public void setInsCodigo(int insCodigo) {
		this.insCodigo = insCodigo;
	}
	
}
