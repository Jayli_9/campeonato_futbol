package ec.jd.Company.modelo;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "camp_serie")
@NamedQuery(name = "CampSerie.findAll", query = "SELECT i FROM CampSerie i")
public class CampSerie implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "SERI_CODIGO")
	private Long seriCodigo;

	@Column(name = "SERI_ESTADO")
	private int seriEstado;

	@Column(name = "SERI_NOMBRE")
	private String seriNombre;

	@ManyToOne
	@JoinColumn(name = "CAMP_CODIGO")
	private CampCampeonato campCampeonato;

	public Long getSeriCodigo() {
		return seriCodigo;
	}

	public void setSeriCodigo(Long seriCodigo) {
		this.seriCodigo = seriCodigo;
	}

	public int getSeriEstado() {
		return seriEstado;
	}

	public void setSeriEstado(int seriEstado) {
		this.seriEstado = seriEstado;
	}

	public String getSeriNombre() {
		return seriNombre;
	}

	public void setSeriNombre(String seriNombre) {
		this.seriNombre = seriNombre;
	}

	public CampCampeonato getCampCampeonato() {
		return campCampeonato;
	}

	public void setCampCampeonato(CampCampeonato campCampeonato) {
		this.campCampeonato = campCampeonato;
	}

	//
	// @Temporal(TemporalType.DATE)
	// @Column(name="BLO_FECHA_CREACION")
	// @JsonFormat(pattern = "yyyy-MM-dd")
	// private Date bloFechaCreacion;
	//
	// @Column(name="BLO_NEMONICO")
	// private String bloNemonico;

}
