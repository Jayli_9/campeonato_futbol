package ec.jd.Company.modelo;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="camp_campeonato")
@NamedQuery(name="CampCampeonato.findAll", query="SELECT i FROM CampCampeonato i")
public class CampCampeonato implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO,generator = "native")
	@GenericGenerator(name="native",strategy = "native")
	@Column(name="CAMP_CODIGO")
	private Long campCodigo;

	@Column(name="CAMP_DESCRIPCION")
	private String campDescripcion;

	@Column(name="CAMP_NOMBRE")
	private String campNombre;
	
	@Column(name="CAMP_ESTADO")
	private int campEstado;

	
	
	public int getCampEstado() {
		return campEstado;
	}

	public void setCampEstado(int campEstado) {
		this.campEstado = campEstado;
	}

	public Long getCampCodigo() {
		return campCodigo;
	}

	public void setCampCodigo(Long campCodigo) {
		this.campCodigo = campCodigo;
	}

	public String getCampDescripcion() {
		return campDescripcion;
	}

	public void setCampDescripcion(String campDescripcion) {
		this.campDescripcion = campDescripcion;
	}

	public String getCampNombre() {
		return campNombre;
	}

	public void setCampNombre(String campNombre) {
		this.campNombre = campNombre;
	}

	

//	@Column(name="BLO_ESTADO")
//	private int bloEstado;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="BLO_FECHA_CREACION")
//	@JsonFormat(pattern = "yyyy-MM-dd")
//	private Date bloFechaCreacion;
//
//	@Column(name="BLO_NEMONICO")
//	private String bloNemonico;
	

}
