package ec.jd.Company.modelo.dto;

public class InstitucionRegimenDTO{
	
	private long insRegCodigo;

	private int insRegEstado;

	private long regCodigo;

	private long insCodigo;

	public long getInsRegCodigo() {
		return insRegCodigo;
	}

	public void setInsRegCodigo(long insRegCodigo) {
		this.insRegCodigo = insRegCodigo;
	}

	public int getInsRegEstado() {
		return insRegEstado;
	}

	public void setInsRegEstado(int insRegEstado) {
		this.insRegEstado = insRegEstado;
	}

	public long getRegCodigo() {
		return regCodigo;
	}

	public void setRegCodigo(long regCodigo) {
		this.regCodigo = regCodigo;
	}

	public long getInsCodigo() {
		return insCodigo;
	}

	public void setInsCodigo(long insCodigo) {
		this.insCodigo = insCodigo;
	}

	
}