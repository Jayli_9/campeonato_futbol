package ec.jd.Company.modelo.constantes;
/**
 * Constantes
 * 
 */
public class Constantes {
	public static final String CODIGO_RESPUESTA = "Ok";
	public static final String CODIGO_RESPUESTA_ERROR = "Error";
	public static final String MENSAJE_OK = "Respuesta exitosa";
	//ruta pruebas servidor
	public static final String UPLOADED_FOLDER_GIEE = "//GIEE_APP//";
	//ruta pruebas locales
//	public static final String UPLOADED_FOLDER_GIEE = "C://GIEE_APP//";
	public static final String CORREO_REPORTE_IES_TRANSPARENCIA = "transparenciame@educacion.gob.ec";
	
}
