package ec.jd.Company.controlador.campeonato;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.jd.Company.modelo.CampSerie;
import ec.jd.Company.modelo.constantes.Constantes;
import ec.jd.Company.modelo.enumerator.EstadoEnum;
import ec.jd.Company.modelo.response.ResponseGenerico;
import ec.jd.Company.servicio.CampSerieServicio;



@RestController
@RequestMapping("private/")
public class CampSerieControlador {
	
	@Autowired
	private CampSerieServicio campSerieServicio;	
	
	/**
	 * REST para listar los cronogramas con estado activo
	 * 
	 * @return objeto response
	 */
	@GetMapping(value = "listarSeries")
	public ResponseGenerico<CampSerie> listarCampeonatos(){
		ResponseGenerico<CampSerie> response=new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		List<CampSerie> listaSeries = campSerieServicio.listarPorEstado(EstadoEnum.ACTIVO.getCodigo());
		response.setListado(listaSeries);
		response.setTotalRegistros((long)listaSeries.size());
		return response;
	}
	/**
	 * REST para listar los cronogramas con estado activo
	 * 
	 * @return objeto response
	 */
	@PostMapping(value = "guardarSerie")
	public ResponseGenerico<CampSerie> guardarCampeonatos(@RequestBody CampSerie campSerie){
		ResponseGenerico<CampSerie> response=new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		CampSerie serieGuardado = campSerieServicio.guardarActualizar(campSerie);
		response.setObjeto(serieGuardado);
		return response;
	}
	
	
}
