package ec.jd.Company.controlador.campeonato;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.jd.Company.modelo.CampEquipo;
import ec.jd.Company.modelo.CampEstadio;
import ec.jd.Company.modelo.constantes.Constantes;
import ec.jd.Company.modelo.dto.ConsultaEquipoDTO;
import ec.jd.Company.modelo.enumerator.EstadoEnum;
import ec.jd.Company.modelo.response.ResponseGenerico;
import ec.jd.Company.servicio.CampEstadioServicio;

@RestController
@RequestMapping("private/")
public class CampEstadioControlador {

	@Autowired
	private CampEstadioServicio campEstadioServicio;

	/**
	 * REST para listar los cronogramas con estado activo
	 * 
	 * @return objeto response
	 */
	@GetMapping(value = "listarEstadios")
	public ResponseGenerico<CampEstadio> listarCampeonatos() {
		ResponseGenerico<CampEstadio> response = new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		List<CampEstadio> listaEstadios = campEstadioServicio.listarPorEstado(EstadoEnum.ACTIVO.getCodigo());
		response.setListado(listaEstadios);
		response.setTotalRegistros((long) listaEstadios.size());
		return response;
	}

	/**
	 * REST para listar los cronogramas con estado activo
	 * 
	 * @return objeto response
	 */
	@PostMapping(value = "guardarEstadio")
	public ResponseGenerico<CampEstadio> guardarCampeonatos(@RequestBody CampEstadio campEstadio) {
		ResponseGenerico<CampEstadio> response = new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		CampEstadio estadioGuardado = campEstadioServicio.guardarActualizar(campEstadio);
		response.setObjeto(estadioGuardado);
		return response;
	}

	@PostMapping(value = "listarEstadiosPaginadoPorCampCodigo")
	public ResponseGenerico<CampEstadio> listarEstadiosPaginadoPorCampCodigo(
			@RequestBody ConsultaEquipoDTO consultaEstadioDTO) {
		ResponseGenerico<CampEstadio> response = new ResponseGenerico<>();
		final Pageable paginacion = PageRequest.of(consultaEstadioDTO.getNumeroPagina(),
		consultaEstadioDTO.getNumeroItems());
		Page<CampEstadio> listaEstadios = campEstadioServicio
				.listarEstadiosPorCampCodigoPaginado(EstadoEnum.ACTIVO.getCodigo(), consultaEstadioDTO.getCampCodigo(),
						paginacion);
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		response.setListado(listaEstadios.getContent());
		response.setTotalRegistros(listaEstadios.getTotalElements());
		response.setTotalRegistrosPagina(Long.valueOf(listaEstadios.getSize()));
		response.setTotalPaginas(Long.valueOf(listaEstadios.getTotalPages()));
		return response;
	}

}
