package ec.jd.Company.controlador.campeonato;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ec.jd.Company.modelo.CampEquipo;
import ec.jd.Company.modelo.constantes.Constantes;
import ec.jd.Company.modelo.dto.ConsultaEquipoDTO;
import ec.jd.Company.modelo.enumerator.EstadoEnum;
import ec.jd.Company.modelo.response.ResponseGenerico;
import ec.jd.Company.servicio.CampEquipoServicio;

@RestController
@RequestMapping("private/")
public class CampEquipoControlador {

	@Autowired
	private CampEquipoServicio campEquipoService;

	/**
	 * REST para listar los cronogramas con estado activo
	 * 
	 * @return objeto response
	 */
	@GetMapping(value = "listarEquipos")
	public ResponseGenerico<CampEquipo> listarCampeonatos() {
		ResponseGenerico<CampEquipo> response = new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		List<CampEquipo> listaEquipos = campEquipoService.listarPorEstado(EstadoEnum.ACTIVO.getCodigo());
		response.setListado(listaEquipos);
		response.setTotalRegistros((long) listaEquipos.size());
		return response;
	}

	/**
	 * REST para listarlos equipos de cauerdo al codigo de un campeonato
	 * 
	 * @return objeto response
	 */
	@GetMapping(value = "listarEquiposPorCampCodigo/{campCodigo}")
	public ResponseGenerico<CampEquipo> listarEquiposPorCampCodigo(@PathVariable("campCodigo") Long camCodigo) {
		ResponseGenerico<CampEquipo> response = new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		List<CampEquipo> listaEquipos = campEquipoService.listarPorEstadoAndCampCodigo(EstadoEnum.ACTIVO.getCodigo(),
				camCodigo);
		response.setListado(listaEquipos);
		response.setTotalRegistros((long) listaEquipos.size());
		return response;
	}

	/**
	 * REST para listar los cronogramas con estado activo
	 * 
	 * @return objeto response
	 */
	@PostMapping(value = "guardarEquipos")
	public ResponseGenerico<CampEquipo> guardarCampeonatos(@RequestBody CampEquipo campEquipo) {
		ResponseGenerico<CampEquipo> response = new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		CampEquipo equipoGuardado = campEquipoService.guardarActualizar(campEquipo);
		response.setObjeto(equipoGuardado);
		return response;
	}

	@PostMapping(value = "listarEquiposPaginadoPorCampCodigo")
	public ResponseGenerico<CampEquipo> listarEquiposPaginadoPorCampCodigo(
			@RequestBody ConsultaEquipoDTO consultaEquipoDTO) {
		ResponseGenerico<CampEquipo> response = new ResponseGenerico<>();
		final Pageable paginacion = PageRequest.of(consultaEquipoDTO.getNumeroPagina(),
				consultaEquipoDTO.getNumeroItems());
		Page<CampEquipo> listaEquipos = campEquipoService
				.listarEquiposPorCampCodigoPaginado(EstadoEnum.ACTIVO.getCodigo(), consultaEquipoDTO.getCampCodigo(),
						paginacion);
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		response.setListado(listaEquipos.getContent());
		response.setTotalRegistros(listaEquipos.getTotalElements());
		response.setTotalRegistrosPagina(Long.valueOf(listaEquipos.getSize()));
		response.setTotalPaginas(Long.valueOf(listaEquipos.getTotalPages()));
		return response;
	}

}
