package ec.jd.Company.controlador.campeonato;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.jd.Company.modelo.CampCampeonato;
import ec.jd.Company.modelo.constantes.Constantes;
import ec.jd.Company.modelo.enumerator.EstadoEnum;
import ec.jd.Company.modelo.response.ResponseGenerico;
import ec.jd.Company.servicio.CampCampeonatoService;

@RestController
@RequestMapping("private/")
public class CampCampeonatoControlador {

	@Autowired
	private CampCampeonatoService camCampeonatoService;

	/**
	 * REST para listar los cronogramas con estado activo
	 * 
	 * @return objeto response
	 */
	@GetMapping(value = "listarCampeonatos")
	public ResponseGenerico<CampCampeonato> listarCampeonatos() {
		ResponseGenerico<CampCampeonato> response = new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		List<CampCampeonato> listaCampeonatos = camCampeonatoService.listarPorEstado(EstadoEnum.ACTIVO.getCodigo());
		response.setListado(listaCampeonatos);
		response.setTotalRegistros((long) listaCampeonatos.size());
		return response;
	}

	@GetMapping(value = "listarCampeonatosPaginado/{numPagina}/{itemsPagina}")
	public ResponseGenerico<CampCampeonato> listarCampeonatosPaginado(@PathVariable("numPagina") int numPagina,
			@PathVariable("itemsPagina") int itemsPagina) {
		ResponseGenerico<CampCampeonato> response = new ResponseGenerico<>();
		final Pageable paginacion = PageRequest.of(numPagina, itemsPagina);
		Page<CampCampeonato> listaCampeonatos = camCampeonatoService
				.listarPorEstadoPaginacion(EstadoEnum.ACTIVO.getCodigo(), paginacion);
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		response.setListado(listaCampeonatos.getContent());
		response.setTotalPaginas(Long.valueOf(listaCampeonatos.getTotalPages()));
		return response;
	}

	/**
	 * REST para listar los cronogramas con estado activo
	 * 
	 * @return objeto response
	 */
	@PostMapping(value = "GuardarCampeonatos")
	public ResponseGenerico<CampCampeonato> guardarCampeonatos(@RequestBody CampCampeonato camCampeonato) {
		ResponseGenerico<CampCampeonato> response = new ResponseGenerico<>();
		response.setCodigoRespuesta(Constantes.CODIGO_RESPUESTA);
		response.setMensaje(Constantes.MENSAJE_OK);
		CampCampeonato campeonatoGuardadod = camCampeonatoService.guardarActualizar(camCampeonato);
		response.setObjeto(campeonatoGuardadod);
		return response;
	}

}
