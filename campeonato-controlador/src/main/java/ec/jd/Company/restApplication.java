package ec.jd.Company;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
@Configuration
public class restApplication extends SpringBootServletInitializer{

    public static void main(String[] args) {
        System.setProperty("server.servlet.context-path", "/campeonato-back");
        SpringApplication.run(restApplication.class, args);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/campeonato-back/private//**").allowedMethods("PUT", "DELETE", "GET", "POST");
            }
        };
    }

}
