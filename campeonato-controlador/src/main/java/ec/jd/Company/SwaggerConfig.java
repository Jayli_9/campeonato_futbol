package ec.jd.Company;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Profile("prod")
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	public Docket apiDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("ec.jd.Company.controlador.campeonato"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(getApiInfo())
				;
	}
	
	private ApiInfo getApiInfo() {
		return new ApiInfo(
				"API-REST - CAMPEONATO 1.0",
				"Descripción Api-Rest campeonatos 1.0",
				"1.0",
				"https://link_servicion_compañia_jd/",
				new Contact("EJEMPLO", "https://ejemplo.com", "correo@ejemplo.com"),
				"LICENSE",
				"LICENSE URL",
				Collections.emptyList()
				);
	}
}
